#include "../../includes/genericphc1.hpp"

inline 
unsigned int factorial(const int n)
{
  unsigned int tmp = 1;
  for (std::size_t i = n; i > 0; --i)
    tmp *= i;

  return tmp;
}

inline
void swap(std::string &s, const unsigned int i, const unsigned int j)
{
  char tmp = s[i];
  s[i] = s[j];
  s[j] = tmp;
}

std::string rec_string_permutation(std::string s ,int i, int j)
{
  if (i == j)
   return s; 
  
  std::cout << s << "\t \n";

  swap(s, i, j);
  rec_string_permutation(s, ++i, j);
  return s;
}

int main(int argc, char** args)
{
  std::string s = "ABC";
  std::cout << "-- Permutations: \n" 
            << rec_string_permutation(s, 1, s.length() - 1) << '\n';

  // std::vector<std::string> v;

  // std::cout << "-- Get All Permutation for string :" << '\n';
  // for (const auto & e : v)
  //   std::cout << e << '\n';

  // abc 
  // acb 
  // bac 
  // bca
  // cab 
  // cba

  return 0;
}
