#include <stdio.h>

void previous_sum_arr(const int n, int array[], int i) {
  if (n < 1 || (i + 1) == n) return;

  ++i;
  array[i] = array[i - 1] + array[i];
  previous_sum_arr(n, array, i);
}

int main(int argc, char* args[]) {
  const int n = 6;
  const int starting_index = 0;
  int array[] = {1, 2, 3, 4, 5, 6};

  previous_sum_arr(n, array, starting_index);

  printf("\t Elements in the array after modification  \n \t \t");
  for (int i = 0; i < n; ++i)
    printf("%d ", array[i]);

  printf("\n");
  return 0;
}
