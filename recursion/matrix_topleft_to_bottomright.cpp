#include <iostream>

#include <string.h>

int** get_matrix_from_heap(const int kWidth, const int kHeight);
void free_matrix(int** matrix, const int kWidth);
void show_matrix(int** matrix, const int kWidth, const int kHeight);

int memo_amount_of_steps(int* matrix[], const int kWidth, const int kHeight,
                         int i, int j, int* cache[]) {
  if (i == (kWidth - 1) || j == (kHeight - 1)) return 1;

  if (cache[i][j] != 0) {
    return cache[i][j];
  } else {
    cache[i][j] =
        memo_amount_of_steps(matrix, kWidth, kHeight, (i + 1), j, cache) +
        memo_amount_of_steps(matrix, kWidth, kHeight, i, (j + 1), cache);
  }

  return cache[i][j];
}

int main(int argc, char* args[]) {
  constexpr int kWidth = 4;
  constexpr int kHeight = 3;
  constexpr int kStartingIndex = 0;

  int** matrix = get_matrix_from_heap(kWidth, kHeight);
  show_matrix(matrix, kWidth, kHeight);

  int** cache = get_matrix_from_heap(kWidth, kHeight);
  int s = memo_amount_of_steps(matrix, kWidth, kHeight, kStartingIndex,
                               kStartingIndex, cache);

  std::cout << "\t memo amount of steps of a " << kWidth << 'x' << kHeight
            << " matrix = " << s << '\n';

  free_matrix(cache, kWidth);
  free_matrix(matrix, kWidth);
  return 0;
}

int** get_matrix_from_heap(const int kWidth, const int kHeight) {
  int** matrix = new int*[kWidth];

  for (int i = 0; i < kWidth; ++i) {
    matrix[i] = new int[kHeight];
    memset(matrix[i], 0, sizeof(int) * kHeight);
  }

  return matrix;
}

void show_matrix(int** matrix, const int kWidth, const int kHeight) {
  for (int i = 0; i < kHeight; ++i) {
    for (int j = 0; j < kWidth; ++j) {
      std::cout << matrix[i][j] << ' ';
    }
    std::cout << '\n';
  }
}

void free_matrix(int** matrix, const int kWidth) {
  for (int i = 0; i < kWidth; ++i) delete[] matrix[i];
  delete[] matrix;
}
