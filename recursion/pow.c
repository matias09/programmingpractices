#include <stdint.h>
#include <stdio.h>

int rec_pow_1(int const m, int const n) {
  if (n < 1) return 1;
  return rec_pow_1(m, n - 1) * m;
}

int rec_pow_2(int const m, int const n) {
  if (n < 1) return 1;

  if ((n % 2) == 0) {
    return rec_pow_2(m * m, (n / 2));
  } else {
    return rec_pow_2(m * m, ((n - 1) / 2)) * m;
  }
}

int main(int argc, char* args[]) {
  const int n = 2;
  const int e = 5;

  int r = rec_pow_1(n, e);
  printf("\t rec_pow_1(%d, %d) = %d \n", n, e, r);

  r = rec_pow_2(n, e);
  printf("\t rec_pow_2(%d, %d) = %d \n", n, e, r);

  return 0;
}
