#include <stdio.h>

int rec_nested(int const n) {
  if (n > 100) {
    return n - 10;
  }

  return rec_nested(rec_nested(n + 11));
}

int main(int argc, char* args[]) {
  const int n = 30;

  int r = rec_nested(n);
  printf("\t %d \n", r);

  return 0;
}
