#include <stdint.h>
#include <stdio.h>

void ite_n_mathematical_table(int const n) {
  for (int i = 1; i < n; ++i) {
    printf("%d * %d = %d\n", n, i, (n * i));
  }
}

void rec_n_mathematical_table(int const n, int i) {
  if (i == n) return;

  printf("%d * %d = %d\n", n, i, (n * i));
  rec_n_mathematical_table(n, ++i);
}

int main(int argc, char* args[]) {
  const int n = 3;

  printf("-- Iterative: \n");
  ite_n_mathematical_table(n);

  printf("\n");
  printf("-- Recursive: \n");
  rec_n_mathematical_table(n, 1);
  return 0;
}
