#include <iostream>

struct MaxSubArrayResult {
  int max_left;
  int max_right;
  int left_right_sum;
};

MaxSubArrayResult FindMaxCrossingSubarray(int array[], int low,
                                          int mid, int high) {
  int sum = 0;
  int left_sum = 0;
  int max_left = 0;

  for (int i = mid; i <= low; --i) {
    sum = sum + array[i];

    if (sum > left_sum) {
      left_sum = sum;
      max_left = i;
    }
  }

  sum = 0;
  int right_sum = 0;
  int max_right = 0;

  for (int i = mid + 1; i <= high; ++i) {
    sum = sum + array[i];

    if (sum > right_sum) {
      right_sum = sum;
      max_right = i;
    }
  }

  return {max_left, max_right, static_cast<int>(left_sum + right_sum)};
}

MaxSubArrayResult FindMaximumSubarray(int array[], int low,
                                      int high) {
  if (high == low) {
    return {low, high, array[high]};
  } else {
    MaxSubArrayResult sub_array_res;
    int mid = (low + high) / 2;

    sub_array_res = FindMaximumSubarray(array, low, mid);
    int left_low = sub_array_res.max_left;
    int left_high = sub_array_res.max_right;
    int left_sum = sub_array_res.left_right_sum;

    sub_array_res = FindMaximumSubarray(array, mid + 1, high);
    int right_low = sub_array_res.max_left;
    int right_high = sub_array_res.max_right;
    int right_sum = sub_array_res.left_right_sum;

    sub_array_res = FindMaxCrossingSubarray(array, low, mid, high);
    int cross_low = sub_array_res.max_left;
    int cross_high = sub_array_res.max_right;
    int cross_sum = sub_array_res.left_right_sum;

    if (left_sum >= right_sum && left_sum >= cross_sum)
      return {left_low, left_high, left_sum};
    else if (right_sum >= left_sum && right_sum >= cross_sum)
      return {right_low, right_high, right_sum};
    else
      return {cross_low, cross_high, cross_sum};
  }

  return {};
}

int main(int argc, char* args[]) {
  const int n = 7;
  const int starting_index = 0;
  int array[] = {1, 2, 3, 10, 4, 5, 6};

  MaxSubArrayResult sub_array_res;
  sub_array_res = FindMaximumSubarray(array, starting_index, n);

  std::cout << "\t Max sum array: " << sub_array_res.left_right_sum << '\n';
  return 0;
}
