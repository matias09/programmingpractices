#include <stdint.h>
#include <stdio.h>

int ite_sum_natural_num(int const n) {
  if (n < 1) return 0;

  int r = 0;
  for (int i = 0; i < n; ++i)
    r += i + 1;

  return r;
}

int rec_sum_natural_num(int const n) {
  if (n < 1) return 0;

  return rec_sum_natural_num(n - 1) + n;
}

int main(int argc, char* args[]) {
  const int n = 5;

  int r = rec_sum_natural_num(n);
  printf("\t rec_sum_natural_num(%d) : %d \n", n, r);

  r = ite_sum_natural_num(n);
  printf("\t ite_sum_natural_num(%d) : %d \n", n, r);

  return 0;
}
