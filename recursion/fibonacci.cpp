#include <stdio.h>
#include <string.h>

int rec_fibonacci(int const n) {
  if (n < 3)
    return 1;
  else
    return rec_fibonacci(n - 1) + rec_fibonacci(n - 2);
}

int rec_memo_fibonacci(int const n, int cache[]) {
  if (n < 3) return 1;

  if (cache[n - 1] == 0)
    cache[n - 1] =
        rec_memo_fibonacci(n - 1, cache) + rec_memo_fibonacci(n - 2, cache);

  return cache[n - 1];
}

int ite_fibonacci(int const n) {
  if (n < 3) return 1;

  int a = 0;
  int r = 1;

  for (int i = 2; i < n; ++i) {
    a = r - a;
    r = r + a;
  }

  return r;
}

int main(int argc, char** args) {
  const int n = 8;

  printf("-- Fib number for %d : %d \n", n, rec_fibonacci(n));

  int* cache = new int[n];
  memset(cache, 0, sizeof(int) * n);

  printf("-- MEMO Fib number for %d : %d \n", n, rec_memo_fibonacci(n, cache));

  printf("-- ITE Fib number for %d : %d \n", n, ite_fibonacci(n));

  delete[] cache;
  return 0;
}
